def Descending_Order(num):
    return int(''.join(reversed(sorted(str(num)))))

assert Descending_Order(0)==0
assert Descending_Order(15) == 51
assert Descending_Order(123456789) == 987654321
