
export class Kata {
    static findLongest(array:number[]):number {
	return array.reduce( (longest, x) => String(x).length > String(longest).length ? x : longest, 0);
    }
}

console.log(`${Kata.findLongest([1,2,3, 10, 4])}==10`);

