"""
see https://www.nayuki.io/page/fast-fibonacci-algorithms

F(2k) == F(k)[2F(k+1)-F(k)]
F(2k+1) == F(k+1)^2 + F(k)^2
"""

memos = {0:0, 1:1}
threshold = 5
def fib(n):
    stack = [n]
    while stack != []:
        x = stack[-1]
        if memos.has_key(x):
            stack.pop()
        else:
            if x>=threshold:
                k = x // 2
                a = k
                b = k + 1
            else:
                a = x-1 if x>= 0 else x+1
                b = x-2 if x>= 0 else x+2
            if not memos.has_key(a):
                stack.append(a)
            if not memos.has_key(b):
                stack.append(b)
            if memos.has_key(a) and memos.has_key(b):
                if x >= threshold:
                    if x % 2 == 0:
                        y = memos[a]*(2*memos[b]-memos[a])
                    else:
                        y = memos[b]**2 + memos[a]**2
                else:   
                    y = fib(a) + fib(b) if x >= 0 else fib(b) - fib(a)
                memos[x] = y
                stack = [i for i in stack if i != x]
    return memos[n]                

assert fib(3) == 2
assert fib(-6) == -8
assert fib(20) == 6765

