DIRECTIONS = [(1,0), (0,1), (-1,0), (0, -1)]
def snail(array):
    direction = 0
    x, y = 0, 0
    visited = set()
    path = []
    rotations = 0
    while rotations < 4:
        path.append(array[y][x])
        visited.add((x,y))
        while rotations < 4:
            dx, dy = DIRECTIONS[direction]
            nx, ny = x+dx, y+dy
            if (nx,ny) in visited or ny >= len(array) or nx >= len(array[ny]) or nx <0 or ny<0: 
                direction = (direction + 1) % len(DIRECTIONS)
                rotations += 1
            else:
                x, y = nx, ny
                rotations =0
                break
    return path

array = [[1,2,3],
         [4,5,6],
         [7,8,9]]
expected = [1,2,3,6,9,8,7,4,5]
assert snail(array)== expected


array = [[1,2,3],
         [8,9,4],
         [7,6,5]]
expected = [1,2,3,4,5,6,7,8,9]
assert snail(array)== expected
