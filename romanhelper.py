values = {1: 'I', 5: 'V', 10: 'X', 50: 'L', 100: 'C', 500: 'D', 1000: 'M', 5000:'?', 10000:'?'}
formats = {0: '', 1: 'p', 2:'pp', 3:'ppp', 4:'ps', 5:'s', 6:'sp', 7:'spp', 8:'sppp', 9:'pq'}
bases = [1000,100,10,1]

class RomanNumerals(object):
    @staticmethod
    def to_roman(num):
        return ''.join(formats[(num//base)%10].replace('p', values.get(base)).replace('s', values.get(base*5)).replace('q', values.get(base*10)) for base in bases)

    @staticmethod
    def from_roman(text):
        invvalues = {v: k for k, v in values.items()}
        invformats = {v: k for k, v in formats.items()}
        cursor = 0
        acc = 0
        while cursor < len(text):
            c = text[cursor]
            for base in bases:
                p = values[base]
                s = values[base*5]
                q = values[base*10]
                start = cursor
                while cursor < len(text) and text[cursor] in p+s+q:
                    cursor += 1
                form = text[start:cursor].replace(p, 'p').replace(q, 'q').replace(s, 's')
                acc += base * invformats[form]
        return acc            
        
golden = [(1000, 'M'), (1, 'I'), (10, 'X'), (3, 'III'), (9, 'IX')]

def test_to_roman():
    for num, rom in golden:
        assert RomanNumerals.to_roman(num) == rom

def test_from_roman():
    for num, rom in golden:
        assert RomanNumerals.from_roman(rom) == num

def test_all():
    for i in range(1,4000):
        rom = RomanNumerals.to_roman(i)
        backi = RomanNumerals.from_roman(rom)
        assert i == backi, rom
