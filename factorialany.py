# https://www.codewars.com/kata/trailing-zeros-in-factorials-in-any-given-integer-base/train/python

def isprime(x):
    return [i for i in range(2, x-1) if x % i == 0] == []

def largest_factor(base):
    prime_factors = [x for x in range(2,base+1) if isprime(x) and base%x == 0]
    print(base, 'factors', prime_factors)
    return prime_factors[-1]

def test_factor():
    assert largest_factor(10) == 5
    assert largest_factor(7) == 7

def trailing_zeros(num, base):
    f = largest_factor(base)
    print(base, f)
    def recur(num):
        x = num // f
        return x + recur(x) if x else 0
    return recur(num)

def test_trailing_zeros_a(): assert trailing_zeros(15, 10) == 3
def test_trailing_zeros_b(): assert trailing_zeros(7, 2) == 4
def test_trailing_zeros_c(): assert trailing_zeros(15, 12) == 5
