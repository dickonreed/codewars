export const dashatize = (num: number):string => String(num).match(/([2468]+)|([13579])/g).join('-');

console.log(dashatize(274));
console.log(dashatize(5311));
console.log(dashatize(6811));
