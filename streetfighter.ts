function mod(n, m) {
  return ((n % m) + m) % m;
}
export function streetFighterSelection(fighters: Array<string[]>, position: number[], moves: string[]) {
  let cursor = [position[0], position[1]];
  let hover = [];
  let deltas = { up: [0,1], down:[0,-1], left:[-1,0], right:[1, 0]};
  for (let move of moves) {
      let delta = deltas[move] || [0,0];
      cursor[1] = Math.max(Math.min(cursor[1]-delta[1], fighters.length-1),0);
      cursor[0] = mod((cursor[0]+delta[0]), (fighters[cursor[1]].length));
      hover.push(fighters[cursor[1]][cursor[0]]);
   }
   return hover;
}

