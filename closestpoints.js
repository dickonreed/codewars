function distanceCombine(a, b) {
    return a.distance <= b.distance ? a : b;
}
function bruteClosestPair(points, calculateDistance) {
    let min = undefined;
    for(let i=0; i<points.length; i++) {
	for (let j=i+1; j<points.length; j++) {
	    dist = calculateDistance(points[i], points[j]);
	    here = {distance:dist, pairs:[points[i], points[j]]};
	    min = min == undefined ? here:distanceCombine(min, here);
	}
    }
    return min;
}
function closestPair(points, calculateDistance) {
    function recur(pointwork) {
	if (pointwork.length < 4) return bruteClosestPair(pointwork, calculateDistance);
	let half = Math.floor(pointwork.length / 2);
	let middle = pointwork[half][0];
	let left = recur(pointwork.slice(0, half));
	let right = recur(pointwork.slice(half, pointwork.length));
	let minimum = distanceCombine(left, right);
	let stripMin = bruteClosestPair(pointwork.filter( (p) => p[0] >= middle - minimum.distance && p[1] <= middle + minimum.distance), calculateDistance);
	if (stripMin != undefined) minimum = distanceCombine(stripMin, minimum);
	return minimum;
    } 
    return recur(points.sort((a,b)=>a[0]-b[0])).pairs;
}

function myCalculateDistance(a, b) {
    let dx = b[0]-a[0];
    let dy = b[1]-a[1];
    return Math.sqrt(dx*dx+dy*dy);
}
exports.testMyCalculateDistance = t => {
    t.equal(myCalculateDistance([0,0], [2,0]), 2);
    t.done();
};
exports.testClosestPair = t => {
    let i = [
  [2,2], // A
  [2,8], // B
  [5,5], // C
  [6,3], // D
  [6,7], // E
  [7,4], // F
  [7,8]  // G
    ];
    t.deepEqual(closestPair([[1,1], [2,2]], myCalculateDistance), [[1,1],[2,2]]);
    t.deepEqual(closestPair(i, myCalculateDistance), [[6,3],[7,4]]);
    t.done();
};

